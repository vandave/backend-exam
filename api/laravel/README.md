## API is using Laravel Framework

Steps to use:
 - run `composer update` to download the vendor folder
 - generate .env file
 - run `php artisan migrate` to migrate the tables in your database.
 - run now the api server in http://localhost:8000