<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => 'api','namespace' => '\App\Http\Controllers'],function($router) {

	/* No need for Authentication */
	Route::post('register', 'RegisterController@store')->name('api.register');
	Route::post('login', 'AuthController@login')->name('api.login');
	Route::post('logout', 'AuthController@logout')->name('api.logout');
	Route::apiResource('posts','PostController')->only(['index','show']);
	Route::apiResource('posts.comments','PostCommentsController')->only(['index','show']);

	/* Authenticated users */
	Route::group(['middleware' => ['auth:api']],function($router){
		Route::apiResource('posts','PostController')->only(['store','update','destroy']);
		Route::apiResource('posts.comments','PostCommentsController')->only(['store','update','destroy']);
	});
});