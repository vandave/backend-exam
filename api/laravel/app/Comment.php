<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'body', 'creator_id', 'parent_id'
    ];

    public function commentable()
    {
        return $this->morphTo();
    }
}
