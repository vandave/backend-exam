<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Post;
use App\Http\Resources\Posts;

class PostController extends Controller
{
    protected $guard = 'api';

    /* Display lists of Posts */
    public function index()
    {
        return Posts::collection(Post::latest()->paginate(15));
    }

    /* Display specific post */
    public function show(Post $post)
    {
        return new Posts($post);
    }
    
    /* Add a Post */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'string|required',
            'content' => 'string|required',
            
        ]);

        $data = [
            'title' => $request->input('title'),
            'content' => $request->input('content'),
            'image' => $request->input('image',null),
            'user_id' => auth()->guard($this->guard)->user()->id
        ];

        $post = Post::create($data);

        return response()->json($post, 201);
    }

    /* Update Post */
    public function update(Request $request, Post $post)
    {
        $request->validate([
            'title' => 'string|required',
            'content' => 'string|required',
            
        ]);

        $post->title = $request->input('title');
        $post->content = $request->input('content');
        $post->image = $request->input('image',null);

        $post->save();

        return new Posts($post);
    }

    /* Delete Comment */
    public function destroy(Post $post)
    {
        if ( $post->delete()) {
            return response()->json(['status' => 'Post deleted successfully!']);
        }

        return response()->json(['status' => 'Post failed to delete!'], 422);
    }
}
