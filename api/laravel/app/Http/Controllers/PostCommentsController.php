<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Post;
use App\Comment;
use App\Http\Resources\Comments;

class PostCommentsController extends Controller
{
    protected $guard = 'api';

    /* Display lists of Comments */
    public function index(Post $post)
    {
        return new Comments($post->comments);
    }    

	/* Display specific comment */
    public function show(Comment $comment)
    {
        return new Comments($comment);
    }

    /* Add a Comment */
    public function store(Request $request, Post $post)
    {
        $request->validate([
            'body' => 'string|required'
            
        ]);

        return new Comments($post->comments()->create([
            'body'=>$request->input('body'),
            'creator_id' => auth()->guard($this->guard)->user()->id
            ])
        );
    }

    /* Update Comment */
    public function update(Request $request, Post $post, Comment $comment)
    {
        $request->validate([
            'body' => 'string|required'
            
        ]);

        $comment->body = $request->input('body');
        $comment->save();

        return new Comments($comment);
    }

    /* Delete Comment */
    public function destroy( Post $post, Comment $comment)
    {
        if ($comment->delete()) {
            return response()->json(['status' => 'Comment deleted successfully!']);
        }

        return response()->json(['status' => 'Comment failed to delete!'],422);
    }
}
