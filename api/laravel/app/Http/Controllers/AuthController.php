<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    protected $guard = 'api';

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => '']);
    }

    /* API login */
    public function login()
    {
        $token = '';
        request()->validate([
            'email' => [
                'required',
                'email',
                function($attribute, $value, $fail){
                    if (!$token=auth($this->guard)->attempt(request(['email', 'password'])))
                        $fail('Incorrect email or password!');
                }
            ],
            'password' => 'required'
        ]);

        $token = auth($this->guard)->attempt(request(['email', 'password']));
        
        return response()->json([
            'token' => $token,
            'token_type' => 'bearer',
            'expires_at' => Carbon::now()->addSeconds(auth($this->guard)->factory()->getTTL() * 60)->format('Y-m-d H:i:s')
        ]);
    }

    /* API logout */
    public function logout()
    {
        auth($this->guard)->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }
}