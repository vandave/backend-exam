<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Comment;
use Spatie\Sluggable\SlugOptions;

class Post extends Model
{
    protected $fillable = [
        'user_id', 'title', 'content', 'image', 
    ];

    public function getSlugOptions()
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug');
    }

    /*
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * Get all of the comments linked to the post
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }
}
